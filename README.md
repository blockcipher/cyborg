Depends on Docopt

go get github.com/docopt/docopt-go

**Start server**
Server must be running on both source and destination using the binding port.

Cyborg.exe server 127.0.0.1

**Sync two folders**

Cyborg.exe sync ip,DirectoryPath ip,DirectoryPath

Example: Cyborg.exe sync 127.0.0.1,c:\Projects 127.0.0.1,c:\Backup\Projects
