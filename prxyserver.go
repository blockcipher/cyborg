// Cyborg project prxyserver.go
package main

import (
	"fmt"
	"io/ioutil"
	"net/rpc"
	"os"
	"path/filepath"
	"strings"
)

//Proxy
type PxyCyborgSrv struct {
	client *rpc.Client
}

func NewPxyCyborgSrv(ip string) PxyCyborgSrv {
	client, _ := rpc.Dial("tcp", fmt.Sprintf("%v:%v", ip, 9000))

	return PxyCyborgSrv{client}
}

func (this PxyCyborgSrv) Message(msg string) {
	err := this.client.Call("CyborgSrv.Message", &msg, nil)
	if err != nil {
		panic(err)
	}
}

func (this PxyCyborgSrv) Sync(src, dest string) {
	s := strings.Split(src, ",")
	d := strings.Split(dest, ",")
	fmt.Println("to", filepath.ToSlash(s[1]))
	fmt.Println("From", filepath.FromSlash(s[1]))
	err := this.client.Call("CyborgSrv.Sync", SyncMsg{s[0], filepath.ToSlash(s[1]), d[0], filepath.ToSlash(d[1])}, nil)
	if err != nil {
		panic(err)
	}
}

func (this PxyCyborgSrv) SvrSync(msg SvrSyncMsg) {
	this.client.Call("CyborgSrv.SvrSync", msg, nil)
}

func (this PxyCyborgSrv) DownloadFile(src, dest string) {
	var dfile []byte
	err := this.client.Call("CyborgSrv.DownloadFile", src, &dfile)
	if err != nil {
		panic(err)
	}

	os.MkdirAll(filepath.Dir(filepath.FromSlash(dest)), 0644)
	ioutil.WriteFile(filepath.FromSlash(dest), dfile, 0644)
}
