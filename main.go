// Cyborg project main.go
package main

import (
	"fmt"
	"github.com/docopt/docopt-go"
	"runtime"
	"strings"
)

func main() {
	runtime.GOMAXPROCS(16)
	args := GetArgs()

	fmt.Println(args)

	if args["server"] == true {
		StartServer(args["<ip>"].(string))
	} else if args["sync"] == true {
		srvr := NewPxyCyborgSrv(strings.Split(args["<source>"].(string), ",")[0])
		srvr.Sync(args["<source>"].(string), args["<destination>"].(string))
	}

	//fmt.Println(args)
}

func GetArgs() map[string]interface{} {
	usage := `Cyborg MD5 Directory Sync
	
	
	Usage:
		Cyborg server <ip>
		Cyborg sync	<source> <destination>
	`
	arguments, _ := docopt.Parse(usage, nil, true, "Cyborg 0.1", false)
	return arguments
}
