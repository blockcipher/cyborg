// Cyborg project main.go
package main

import (
	"crypto/md5"
	//"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	//"strings"
	"sync"
	//"time"
)

func GetFiles(root string) map[string][]byte {

	outMap := make(map[string][]byte)
	hashed := make(chan struct {
		path string
		hash []byte
	})

	go func() {
		var wg sync.WaitGroup
		//Hash Files returned from
		for f := range recursiveFiles(root) {

			wg.Add(1)
			go func(x string) {

				defer wg.Done()

				hashed <- struct {
					path string
					hash []byte
				}{filepath.ToSlash(x), HashFile(x)}
			}(f)
		}
		wg.Wait()
		close(hashed)
	}()

	for fh := range hashed {

		outMap[fh.path] = fh.hash
	}

	//HashFileChan(recursiveFiles(root), func(x struct {
	//	path string
	//	hash []byte
	//}) {
	//	fmt.Println(x.path)
	//	outMap[x.path] = x.hash
	//})
	//fmt.Println("1")
	return outMap
}

func HashFileChan(paths chan string, f func(struct {
	path string
	hash []byte
})) chan struct {
	path string
	hash []byte
} {

	hashed := make(chan struct {
		path string
		hash []byte
	})
	//var wg sync.WaitGroup

	go func() {
		for f := range paths {
			func(x string) {
				hashed <- struct {
					path string
					hash []byte
				}{x, HashFile(x)}
			}(f)
		}
		close(hashed)
	}()
	return hashed
}

func recursiveFiles(root string) chan string {
	fls := make(chan string)

	go func() {
		filepath.Walk(root, func(path string, fi os.FileInfo, _ error) error {
			if !fi.IsDir() {
				fls <- path
			}
			return nil
		})
		close(fls)
	}()
	return fls
}

func HashFile(path string) []byte {
	file, _ := ioutil.ReadFile(path)
	m5 := md5.New()
	m5.Write(file)
	return m5.Sum(nil)
}

func TrimLeading(base string, lst map[string][]byte) map[string][]byte {

	mout := make(map[string][]byte)

	for k, v := range lst {

		rel, _ := filepath.Rel(base, k)

		//fmt.Println(k)
		//fmt.Println(base)
		//fmt.Println(strings.TrimLeft(k, base))
		//fmt.Println("")
		//fmt.Println(rel)
		//fmt.Println("")
		//time.Sleep(5.0 * time.Second)

		//mout[strings.TrimLeft(k, base)] = v
		mout[rel] = v
	}

	return mout
}
