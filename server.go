// Cyborg project server.go
package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"net/rpc"
	"os"
	"sync"
	"time"
)

func StartServer(ip string) {
	lis, error := net.Listen("tcp", fmt.Sprintf("%v:%v", ip, 9000))
	if error != nil {
		panic("Start server")
	}

	rpc.Register(new(CyborgSrv))

	for {
		conn, _ := lis.Accept()
		go rpc.ServeConn(conn)
	}
}

//Server
type CyborgSrv struct {
}

type SyncMsg struct {
	Source         string
	SourceDir      string
	Destination    string
	DestinationDir string
}

type SvrSyncMsg struct {
	OrigMsg SyncMsg
	Files   map[string][]byte
}

func (this *CyborgSrv) DownloadFile(file string, result *[]byte) error {
	fle, _ := ioutil.ReadFile(file)
	*result = fle
	return nil
}

func (this *CyborgSrv) Message(msg string, result *string) error {
	fmt.Println(msg)
	return nil
}

func (this *CyborgSrv) Sync(sMsg SyncMsg, result *string) error {
	start := time.Now()
	srcFiles := GetFiles(sMsg.SourceDir)

	s := NewPxyCyborgSrv(sMsg.Destination)
	s.SvrSync(SvrSyncMsg{sMsg, srcFiles})

	fmt.Println(time.Since(start))
	return nil
}

func (this *CyborgSrv) SvrSync(svrMsg SvrSyncMsg, result *string) error {
	destFils := GetFiles(svrMsg.OrigMsg.DestinationDir)

	cpy, del := CompareMap(TrimLeading(svrMsg.OrigMsg.SourceDir, svrMsg.Files), TrimLeading(svrMsg.OrigMsg.DestinationDir, destFils))

	s := NewPxyCyborgSrv(svrMsg.OrigMsg.Source)
	var wg sync.WaitGroup

	fmt.Println("Delete")
	for d1 := range del {
		wg.Add(1)
		go func(d string) {
			defer wg.Done()
			fmt.Println(d)
			os.Remove(svrMsg.OrigMsg.DestinationDir + "\\" + d)
		}(d1)
	}

	fmt.Println("Copy")
	//for c1 := range cpy {
	//	wg.Add(1)

	//	go func(c string) {
	//		defer wg.Done()
	//		fmt.Println(c)
	//		//fmt.Scan()
	//		s.DownloadFile(svrMsg.OrigMsg.SourceDir+"/"+c, svrMsg.OrigMsg.DestinationDir+"/"+c)

	//	}(c1)
	//}

	//Download at a time
	for i := 0; i < 4; i++ {
		go func() {
			for c1 := range cpy {
				wg.Add(1)
				defer wg.Done()
				fmt.Println(c1)
				//fmt.Scan()
				s.DownloadFile(svrMsg.OrigMsg.SourceDir+"/"+c1, svrMsg.OrigMsg.DestinationDir+"/"+c1)
			}
		}()
	}

	wg.Wait()

	return nil
}

func CompareMap(source map[string][]byte, destination map[string][]byte) (chan string, chan string) {
	cpy := make(chan string)
	del := make(chan string)

	go func() {
		for key := range destination {
			if source[key] == nil {
				del <- key
			}
		}
		close(del)
	}()

	go func() {
		for key, val := range source {
			if destination[key] == nil || !bytes.Equal(destination[key], val) {
				cpy <- key
			}
		}
		close(cpy)
	}()

	return cpy, del
}
